/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/medidor_distancia_ultrasonido.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	int16_t distance_cm;
	int16_t distance_in;
	SystemClockInit(); /* Llamar siempre que se trabaja con tiempos*/

	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	while(1){
		distance_cm=HcSr04ReadDistanceCentimeters();
	    distance_in=HcSr04ReadDistanceInches();
	}

	return 0;
}

/*==================[end of file]============================================*/

