/*! @mainpage Contador Objetos Cinta Transportadora
 *
 * \section genDesc Object counter on conveyor belt
 *
 * El programa está constantemente contando líneas cuando las detecta y luego muestra cada 1 segundo los datos.
 *
 * \section hardConn Hardware Connection
 *
 * |    TCRT5000    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	 T_COL0		|
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 * |    ITSE0803    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	   D1	 	| 	   LCD1	    |
 * | 	   D2	 	| 	   LCD2	    |
 * | 	   D3	 	| 	   LCD3	    |
 * | 	   D4	 	| 	   LCD4	    |
 * | 	  SEL_0	 	| 	  GPIO1	    |
 * | 	  SEL_1	 	| 	  GPIO3	    |
 * | 	  SEL_2	 	| 	  GPIO5	    |
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_objetos_cinta_transportadora_proyecto3.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/** @def BIT0
 * @brief comparison mask
 */
#define BIT0 0
/** @def BIT1
 * @brief comparison mask
 */
#define BIT1 1
/** @def BIT2
 * @brief comparison mask
 */
#define BIT2 2
/** @def BIT3
 * @brief comparison mask
 */
#define BIT3 3
/** @def ON
 * @brief mask encendido
 */
#define ON 1
/** @def OFF
 * @brief mask apagado
 */
#define OFF 0
/*==================[internal data definition]===============================*/

gpio_t PINS[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5}; /**< Connection ports for the display LCD*/
uint8_t contador; /**< Count*/
/* Banderas*/
bool active=true; /**< Active mask*/
bool hold=false; /**< Hold mask*/
bool reset=false; /**< Reset mask*/
volatile uint8_t encendido = ON; /**< Encendido timer*/

/*==================[internal functions declaration]=========================*/

/**@fn void contadorObjetos()
 * @brief Object counter function
 * @return None
 */
void contadorObjetos(){
	static bool estado_actual=0;
	static bool estado_anterior=0;

	estado_actual=Tcrt5000State();
	if(estado_actual==1 && estado_anterior==0){
	    contador++;
	}
	estado_anterior=estado_actual;
	if(contador > 15){ /*Maximo valor que puede tomar el contador es 15, luego sigue mostrando 15*/
		contador=15;
	}
}

/**@fn void manejoLeds()
 * @brief Function for managing led in binary
 * @return None
 */
void manejoLeds(){

	if ((contador & 1<<BIT0) == 1<<BIT0){
		LedOn(LED_3);
	}
	else{
		LedOff(LED_3);
	}
	if ((contador & 1<<BIT1) == 1<<BIT1){
		LedOn(LED_2);
	}
	else{
		LedOff(LED_2);
	}
	if ((contador & 1<<BIT2) == 1<<BIT2){
		LedOn(LED_1);
	}
	else{
		LedOff(LED_1);
	}
	if ((contador & 1<<BIT3) == 1<<BIT3){
		LedOn(LED_RGB_B);
	}
	else{
		LedOff(LED_RGB_B);
	}

}

/**@fn void manejoDisplay()
 * @brief Function to show counter on display LCD
 * @return None
 */
void manejoDisplay(){
	ITSE0803DisplayValue(contador);
}
/**@fn void functionSwitch_1()
 * @brief Function controlling switch 1
 * @return None
 */
void functionSwitch_1(){
	active=!active;
}
/**@fn void functionSwitch_2()
 * @brief Function controlling switch 2
 * @return None
 */
void functionSwitch_2(){
	hold=!hold;
}
/**@fn void functionSwitch_3()
 * @brief  Function controlling switch 3
 * @return None
 */
void functionSwitch_3(){
	reset=true;
}
/**@fn void doBlink()
 * @brief  Function controlling timer
 * @return None
 */
void doBlink(void)
{
	encendido=ON;
}
/**@fn void doPuerto()
 * @brief  Function controlling port
 * @return None
 */
void doPuerto(void){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC,&dato);
	if(dato=='O'){ /* Funciona como la tecla 1*/
		active=!active;
	}
	if(dato=='H'){
		hold=!hold; /* Funciona como la tecla 2*/
	}
	if(dato=='0'){
		reset=true; /* Funciona como la tecla 3*/
	}
}

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&doBlink}; /**< Typedef timer*/
serial_config my_puerto = {SERIAL_PORT_PC,115200,&doPuerto}; /**< Typedef port*/

/**@fn void sisInit()
 * @brief  Function initializes the system
 * @return None
 */
void sisInit(void)
{
	SystemClockInit();
	Tcrt5000Init(GPIO_T_COL0);
	ITSE0803Init(PINS);
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	contador=0;
	UartInit(&my_puerto);

}
/*==================[external functions definition]==========================*/

int main(void){

	/* Inicialización */
	sisInit();

	SwitchActivInt(SWITCH_1,functionSwitch_1);
	SwitchActivInt(SWITCH_2,functionSwitch_2);
	SwitchActivInt(SWITCH_3,functionSwitch_3);

    while(1){
    //	if(encendido==ON){
    			/* bandera encendido*/
				if(active==true){
					contadorObjetos();
					/* bandera hold */
					if(encendido==ON){
						if(hold==false){
							manejoLeds();
							manejoDisplay();
							UartSendString(SERIAL_PORT_PC,UartItoa(contador,10));
							UartSendString(SERIAL_PORT_PC," ");
							UartSendString(SERIAL_PORT_PC,"lineas\r\n");
						}
						else{
						}
					}
					encendido=OFF;
				}
    			else{
    			}
    	        /* bandera reset */
    	        if(active==true && reset==true){
    	        	contador=0;
    	        	contadorObjetos();
    	        	manejoLeds();
    	        	manejoDisplay();
    	        	reset=false;
    	        }
    	        else{
    	        }
    }
	return 0;

}

/*==================[end of file]============================================*/

