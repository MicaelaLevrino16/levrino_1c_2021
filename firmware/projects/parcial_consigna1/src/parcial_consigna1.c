/*! @mainpage Parcial Consigna 1
 *
 * \section genDesc General Description
 *
 * Odómetro (medidor de distancia)
 *
 * \section hardConn Hardware Connection
 *
 * |    TCRT5000    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	 T_COL0		|
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

/*==================[inclusions]=============================================*/
#include "parcial_consigna1.h"       /* <= own header */
#include "Tcrt5000.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

/** @def BAUD_RATE
 * @brief Número de bits por segundo
 */
#define BAUD_RATE 115200
/** @def MAX_ABERTURAS
 * @brief Máximo número de aberturas que posee el disco
 */
#define MAX_ABERTURAS 20
/** @def RADIO_DISCO
 * @brief Radio del disco
 */
#define RADIO_DISCO 16
/** @def PI
 * @brief Definición del valor del número pi
 */
#define PI 3.14159

/*==================[internal data definition]===============================*/

uint8_t contador; /**< Contador de pulsos*/
float distancia_recorrida; /**< Distancia recorrida [cm]*/
volatile bool encendido_timer = true; /**< Encendido timer*/
bool active=true; /**< Bandera encendido y apagado*/
bool reset=false; /**< Bandera reseteo de la cuenta*/

/*==================[internal functions declaration]=========================*/

/**@fn void contadorPulsos()
 * @brief Función que cuenta los pulsos generados por un disco perforado.
 * @return None
 */
void contadorPulsos(){
	static bool estado_actual = 0;
	static bool estado_anterior = 0;

	estado_actual = Tcrt5000State();
	/* Cuento las perforaciones, el driver lee 0 cuando no tiene un objeto en frente (perforación) y 1 cuando está el disco**/
	if (estado_actual == 0 && estado_anterior == 1) {
		contador++;
	}
	estado_anterior=estado_actual;

	if (contador == MAX_ABERTURAS){
		contador=0;
	}
}
/**@fn void medidorDistancia()
 * @brief Función que mide la distancia recorrida.
 * @return None
 */
void medidorDistancia(){
	/*Una vuelta del disco en cm puede medirse como 2*pi*radio*/
	distancia_recorrida=(float)(2*PI*RADIO_DISCO)/contador;
}
/**@fn void functionSwitch1()
 * @brief Función que controla la tecla 1 para el encendido.
 * @return None
 */
void functionSwitch1(){
	active = true;
}
/**@fn void functionSwitch2()
 * @brief Función que controla la tecla 2 para el apagado.
 * @return None
 */
void functionSwitch2(){
	active = false;
}
/**@fn void functionSwitch3()
 * @brief Función que controla la tecla 3 para el reseteo de la cuenta.
 * @return None
 */
void functionSwitch3(){
	reset = true;
}
/**@fn void doMedicion()
 * @brief  Función que controla el timer.
 * @return None
 */
void doMedicion(void)
{
	encendido_timer=true;
}

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&doMedicion}; /**< Typedef timer*/
serial_config my_puerto = {SERIAL_PORT_PC,BAUD_RATE,NO_INT}; /**< Typedef port*/

/**@fn void sistemInit()
 * @brief  Función que realiza las inicializaciones del sistema
 * @return None
 */
void sistemInit(void)
{
	SystemClockInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_puerto);
	SwitchesInit();

	contador=0;
	distancia_recorrida=0;
}

/*==================[external functions definition]==========================*/
/**@fn void main()
 * @brief  Función main de aplicación del proyecto
 * @return 0
 */
int main(void){

	/* Inicialización */
	sistemInit();

	/* Control de teclas por interrupciones*/
	SwitchActivInt(SWITCH_1, functionSwitch1);
	SwitchActivInt(SWITCH_2, functionSwitch2);
	SwitchActivInt(SWITCH_3, functionSwitch3);

    while(1){

    	if(active == true){
    		contadorPulsos();
    		medidorDistancia();
    		if(encendido_timer==true){
    			UartSendString(SERIAL_PORT_PC, UartItoa(distancia_recorrida, 10));
    			UartSendString(SERIAL_PORT_PC, "cm\r\n");
    		}
    		encendido_timer=false;
    	}
    	/* bandera reset */
		if (active == true && reset == true) {
			contador = 0;
			contadorPulsos();
			medidorDistancia();
			reset = false;
		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/

