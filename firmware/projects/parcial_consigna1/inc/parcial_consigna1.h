/*! @mainpage Parcial Consigna 1
 *
 * \section genDesc General Description
 *
 * Odómetro (medidor de distancia)
 *
 * \section hardConn Hardware Connection
 *
 * |    TCRT5000    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	 T_COL0		|
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

#ifndef _PARCIAL_CONSIGNA1_H
#define _PARCIAL_CONSIGNA1_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIAL_CONSIGNA1_H */

