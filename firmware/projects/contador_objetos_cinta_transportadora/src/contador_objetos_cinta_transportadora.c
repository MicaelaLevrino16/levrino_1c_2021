/*! @mainpage Contador Objetos Cinta Transportadora
 *
 * \section genDesc Object counter on conveyor belt
 *
 * Counts objects detected by infrared sensor in binary showing in LEDs, and in decimal in LCD display.
 *
 * \section hardConn Hardware Connection
 *
 * |    TCRT5000    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	 T_COL0		|
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 * |    ITSE0803    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	   D1	 	| 	   LCD1	    |
 * | 	   D2	 	| 	   LCD2	    |
 * | 	   D3	 	| 	   LCD3	    |
 * | 	   D4	 	| 	   LCD4	    |
 * | 	  SEL_0	 	| 	  GPIO1	    |
 * | 	  SEL_1	 	| 	  GPIO3	    |
 * | 	  SEL_2	 	| 	  GPIO5	    |
 * | 	  +5V	 	| 	   +5V	    |
 * | 	  GND	 	| 	   GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/04/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_objetos_cinta_transportadora.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

/** @def BIT0
 * @brief comparison mask
 */
#define BIT0 0
/** @def BIT1
 * @brief comparison mask
 */
#define BIT1 1
/** @def BIT2
 * @brief comparison mask
 */
#define BIT2 2
/** @def BIT3
 * @brief comparison mask
 */
#define BIT3 3

/*==================[internal data definition]===============================*/

gpio_t PINS[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5}; /**< Connection ports for the display LCD*/
uint8_t contador; /**< Count*/

/*==================[internal functions declaration]=========================*/

/**@fn void contadorObjetos()
 * @brief Object counter function
 * @return None
 */
void contadorObjetos(){
	static bool estado_actual=0;
	static bool estado_anterior=0;

	estado_actual=Tcrt5000State();
	if(estado_actual==1 && estado_anterior==0){
	    contador++;
	}
	estado_anterior=estado_actual;
	if(contador > 15){ /*Maximo valor que puede tomar el contador es 15, luego sigue mostrando 15*/
		contador=15;
	}
}

/**@fn void manejoLeds()
 * @brief Function for managing led in binary
 * @return None
 */
void manejoLeds(){

	if ((contador & 1<<BIT0) == 1<<BIT0){
		LedOn(LED_3);
	}
	else{
		LedOff(LED_3);
	}
	if ((contador & 1<<BIT1) == 1<<BIT1){
		LedOn(LED_2);
	}
	else{
		LedOff(LED_2);
	}
	if ((contador & 1<<BIT2) == 1<<BIT2){
		LedOn(LED_1);
	}
	else{
		LedOff(LED_1);
	}
	if ((contador & 1<<BIT3) == 1<<BIT3){
		LedOn(LED_RGB_B);
	}
	else{
		LedOff(LED_RGB_B);
	}

}

/**@fn void manejoDisplay()
 * @brief Function to show counter on display LCD
 * @return None
 */
void manejoDisplay(){
	ITSE0803DisplayValue(contador);
}
/*==================[external data definition]===============================*/

/*Mostrar cantidad de objetos contados utilizando los leds como un contador binario
 *en el cual el LED_RGB_B (Azul) como el bit 2 a la 3, el LED_1 como 2 a la 2, el LED_2 como 2 a la 1 y el LED_3 como 2 a la 0*/

/*==================[external functions definition]==========================*/

int main(void){

	/* Inicializaciones */
	Tcrt5000Init(GPIO_T_COL0);
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	ITSE0803Init(PINS);

	contador=0;
	uint8_t teclas;

	/* Banderas*/
	bool active=true;
	bool hold=false;
	bool reset=false;

    while(1){
    	teclas = SwitchesRead();
    	switch(teclas){
    	    case SWITCH_1:
    	    	active=!active;
    	    break;
    	    case SWITCH_2:
    	    	hold=true;
    	    break;
    	    case SWITCH_3:
    	    	reset=true;
    	    break;
    	}
    	/* bandera encendido*/
		if(active==true){
			contadorObjetos();
			/* bandera hold */
			if(hold==false){
				manejoLeds();
				manejoDisplay();
			}
			else{
			}
		}
    	else{
    	}
       /* bandera reset */
       if(active==true && reset==true){
    	   contador=0;
    	   contadorObjetos();
    	   manejoLeds();
    	   manejoDisplay();
    	   reset=false;
       }
       else{
       }
       DelayMs(200);
    }
	return 0;

}

/*==================[end of file]============================================*/

