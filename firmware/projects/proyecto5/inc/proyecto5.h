/*! @mainpage Proyecto 5: Acelerómetro aplicado a detección de movimiento de cabeza
 *
 * \section genDesc General Description
 *
 * La aplicación permite el control de un mousse mediante la detección de diferentes movimientos de cabeza para personas cuadripléjicas.
 *
 * \section hardConn Hardware Connection
 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_COL0 	|
 * |   	  GS2	 	| 	 T_COL1		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

#ifndef _PROYECTO5_H
#define _PROYECTO5_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO5_H */

