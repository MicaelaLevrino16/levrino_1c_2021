/*! @mainpage Proyecto 5: Acelerómetro aplicado a detección de movimiento de cabeza
 *
 * \section genDesc General Description
 *
 * La aplicación permite el control de un mousse mediante la detección de diferentes movimientos de cabeza para personas cuadripléjicas.
 *
 * \section hardConn Hardware Connection
 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_COL0 	|
 * |   	  GS2	 	| 	 T_COL1		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

/*==================[inclusions]=============================================*/
#include "proyecto5.h"       /* <= own header */
#include "MMA7260Q.h"
#include "timer.h"
#include "uart.h"
#include "systemclock.h"
#include "switch.h"
#include "led.h"
/*==================[macros and definitions]=================================*/
/** @def BAUD_RATE
 * @brief Número de unidades de señal por segundo
 */
#define BAUD_RATE 115200
/** @def UMBRAL_SUP
 * @brief Umbral de valores superior
 */
#define UMBRAL_SUP 6900
/** @def UMBRAL_INF
 * @brief Umbral de valores inferior
 */
#define UMBRAL_INF 5250
/** @def SAMPLE_DISTANCE
 * @brief Distancia entre muestras en segundos
 */
#define SAMPLE_DISTANCE 0.005
/** @def PI
 * @brief Definición del valor del número pi
 */
#define PI 3.14159
/** @def FREC_CORTE
 * @brief Valor de la frecuencia de corte
 */
#define CUTTOF_FREQ 1 /* 1Hz*/

/*==================[internal data definition]===============================*/

bool reading_data = false; /**< Bandera para la lectura de valores analógicos*/
float value_x,value_y,value_z; /**< Definicion de los valores para los 3 canales*/
bool flag_ahead = false; /**< Bandera movimiento hacia delante*/
bool flag_right = false; /**< Bandera movimiento a la derecha*/
bool flag_left = false; /**< Bandera movimiento a la izquierda*/
bool activate_graph = false; /**< Bandera para activar gráfica del osciloscopio*/

/*==================[internal functions declaration]=========================*/

/**@fn void analogToDigital()
 * @brief  Función que inicializa la conversión de analógico a digital
 * @return None
 */
void analogToDigital(void){
	reading_data = true;
}
/**@fn void detectAhead()
 * @brief  Función que detecta el movimiento hacía adelante
 * @return None
 */
void detectAhead(){
	if(value_x > UMBRAL_SUP && flag_ahead==false){
		UartSendString(SERIAL_PORT_PC,"Se presiona el mousse.\r\n");
		LedOn(LED_1);
		flag_ahead=true;
	}
}
/**@fn void detectRight()
 * @brief  Función que detecta el movimiento hacía la derecha
 * @return None
 */
void detectRight(){
	if(value_y > UMBRAL_SUP && flag_right==false){
		UartSendString(SERIAL_PORT_PC,"Se mueve el mouse hacia la derecha.\r\n");
		LedOn(LED_2);
		flag_right=true;
	}
}
/**@fn void detectLeft()
 * @brief  Función que detecta el movimiento hacía la izquierda
 * @return None
 */
void detectLeft(){
	if(value_y < UMBRAL_INF && flag_left==false){
		UartSendString(SERIAL_PORT_PC,"Se mueve el mouse hacia la izquierda.\r\n");
		LedOn(LED_3);
		flag_left=true;
	}
}
/** @fn void lowPassFilter(float valor_entrada, float valor_anterior)
 * @brief Función que aplica un filtro pasa-bajos recibiendo el valor de la señal original y el valor anterior filtrado, y devuelve el valor filtrado
 * @param[in] valor_entrada
 * @param[in] valor_anterior
 * @return valor de salida filtrado
 */
float lowPassFilter(float valor_entrada, float valor_anterior){
	float alpha;
	float rc;
	float valor_salida;
	/* Calculo rc*/
	rc=1/(2*PI*CUTTOF_FREQ);
	/* Calculo alpha*/
	alpha = SAMPLE_DISTANCE/(rc+SAMPLE_DISTANCE);
	/* Filtro pasa-bajos*/
	valor_salida = (float)valor_anterior + alpha*(float)(valor_entrada-valor_anterior);
	return valor_salida;
}
/** @fn void detectionKeep()
 * @brief Función que mantiene el valor de lectura de la detección del movimiento en determinado tiempo controlado por el timer B
 * @param[in] No hay parámetro
 * @return None
 */
void detectionKeep(){
	if(flag_ahead){
		flag_ahead=false;
		LedOff(LED_1);
	}
	if(flag_right){
		flag_right=false;
		LedOff(LED_2);
	}
	if(flag_left){
		flag_left=false;
		LedOff(LED_3);
	}
}
/**@fn void functionSwitch1()
 * @brief  Función que controla la tecla 1 de la placa. Activa la visualización del gráfico.
 * @return None
 */
void functionSwitch1(){
	activate_graph = true;
}
/**@fn void functionSwitch2()
 * @brief  Función que controla la tecla 2 de la placa. Desactiva la visualización del gráfico.
 * @return None
 */
void functionSwitch2(){
	activate_graph = false;
}

/*==================[external data definition]===============================*/

timer_config my_timer_AD = {TIMER_A,5,analogToDigital}; /**< Typedef timer analógico/digital*/
serial_config my_port = {SERIAL_PORT_PC,BAUD_RATE,NO_INT}; /**< Typedef control del puerto*/
timer_config my_timer_det = {TIMER_B,1000,detectionKeep}; /**< Typedef control mantener la detección movimiento*/

/**@fn void systemInit()
 * @brief  Función que contiene las funciones que inicializan el sistema
 * @return None
 */
void systemInit(void)
{
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	/*Arranca con todos los leds apagados*/
	LedsOffAll();
	MMA7260QInit(GPIO_T_COL0,GPIO_T_COL1);
	TimerInit(&my_timer_AD);
	TimerStart(TIMER_A);
	TimerInit(&my_timer_det);
	TimerStart(TIMER_B);
	UartInit(&my_port);
}

/*==================[external functions definition]==========================*/

/**@fn void main()
 * @brief  Función main de aplicación del proyecto
 * @return 0
 */
int main(void){

	systemInit();

	/* Manejo de teclas de la placa por interrupciones */
	SwitchActivInt(SWITCH_1,functionSwitch1);
	SwitchActivInt(SWITCH_2,functionSwitch2);

	float output_data_x,output_data_y,output_data_z;
	float previous_data_x,previous_data_y,previous_data_z;

    while(1){
    	if(reading_data){

    		/* Lectura de los 3 canales*/
    		value_x = ReadXValue();
    		value_y = ReadYValue();
    		value_z = ReadZValue();

    		/* Aplicación del filtro pasa bajos a los 3 canales*/
    		output_data_x=lowPassFilter(value_x,previous_data_x);
    		output_data_y=lowPassFilter(value_y,previous_data_y);
    		output_data_z=lowPassFilter(value_z,previous_data_z);

    		/* Multiplico el valor por 1000 porque es muy pequeño y le sumo 6000 (ahora es mi 0) porque no puedo ver valores negativos */
    		/* 7000=+1g ; 5000=-1g */
    		value_x = (float) (output_data_x * 1000 + 6000);
    		value_y = (float) (output_data_y * 1000 + 6000);
    		value_z = (float) (output_data_z * 1000 + 6000);

    		/* Detección de movimientos*/
			detectAhead();
			detectRight();
			detectLeft();

			if (activate_graph) {
				UartSendString(SERIAL_PORT_PC, UartItoa(value_x, 10));
				UartSendString(SERIAL_PORT_PC, ",");
				UartSendString(SERIAL_PORT_PC, UartItoa(value_y, 10));
				UartSendString(SERIAL_PORT_PC, ",");
				UartSendString(SERIAL_PORT_PC, UartItoa(value_z, 10));
				UartSendString(SERIAL_PORT_PC, "\r");
			}

			previous_data_x = output_data_x;
			previous_data_y = output_data_y;
			previous_data_z = output_data_z;

			reading_data = false;
		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/

