/*! @mainpage Osciloscopio
 *
 * \section genDesc General Description
 *
 * Permite digitalizar una señal analogica y transmitirla a un graficador de puerto serie de la PC. Además permite aplicar un filtro pasa-bajos a la señal
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIÓMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	   GNDA  	|
 * | 	  PIN2	 	| 	   CH1	    |
 * | 	  PIN3	 	| 	   DAC		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */
/*==================[inclusions]=============================================*/
#include "../inc/osciloscopio_proyecto4.h"       /* <= own header */
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "systemclock.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
/** @def BUFFER_SIZE
 * @brief Tamaño del vector de ECG
 */
#define BUFFER_SIZE 231
/** @def BAUD_RATE
 * @brief Número de unidades de señal por segundo
 */
#define BAUD_RATE 115200
/** @def SAMPLE_DISTANCE
 * @brief Distancia entre muestras en segundos
 */
#define SAMPLE_DISTANCE 0.002 /*dt[seg]*/
/** @def PI
 * @brief Definición del valor del número pi
 */
#define PI 3.14159
/** @def FREQUENCY_MAX
 * @brief Valor de frecuencia máxima que puede tomar la señal ECG
 */
#define FREQUENCY_MAX 50
/** @def FREQUENCY_MIN
 * @brief Valor de frecuencia mínima que puede tomar la señal ECG
 */
#define FREQUENCY_MIN 5
/** @def DELTA_FREC
 * @brief Pasos en los que se aumenta o disminuye el valor de la frecuencia de corte
 */
#define DELTA_FREC 1

/*==================[internal data definition]===============================*/

uint16_t value; /**< Valores que toma el ECG*/
uint8_t indice = 0; /**< Indice para el recorrido del vector de ecg*/
uint16_t frecuencia_corte = 25; /**< Frecuencia de corte inicializada con un valor medio*/
bool activate_filter = false; /**< Bandera para activar y desactivar el filtro*/
bool reading_data = false; /**< Bandera para la lectura de datos*/

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
}; /**< Señal de ecg provista por la cátedra*/

/*==================[internal functions declaration]=========================*/

/**@fn void analogToDig()
 * @brief  Función que inicializa la conversión de analógico a digital
 * @return None
 */
void analogToDig(void){
	AnalogStartConvertion();
}
/**@fn void digToAnalog()
 * @brief  Función que convierte a analógica la señal digital
 * @return None
 */
void digToAnalog(void){
	indice ++;
	AnalogOutputWrite(ecg[indice]);
	if(indice==(BUFFER_SIZE-1)){
		indice=0;
	}
}
/**@fn void doAnalogToDig()
 * @brief  Función que lee valores analógicos y activa la bandera para la conversión y osbervación en digital
 * @return None
 */
void doAnalogToDig(void){
	AnalogInputRead(CH1,&value);
	reading_data = true;
}
/**@fn void functionSwitch1()
 * @brief  Función que controla la tecla 1 de la placa. Activa el filtro
 * @return None
 */
void functionSwitch1(){
	activate_filter = true;
}
/**@fn void functionSwitch2()
 * @brief  Función que controla la tecla 2 de la placa. Desactiva el filtro
 * @return None
 */
void functionSwitch2(){
	activate_filter = false;
}
/**@fn void functionSwitch3()
 * @brief  Función que controla la tecla 3 de la placa. Disminuye la frecuencia de corte
 * @return None
 */
void functionSwitch3(){
	frecuencia_corte = frecuencia_corte - DELTA_FREC;
	if(frecuencia_corte < FREQUENCY_MIN){
		frecuencia_corte = FREQUENCY_MIN;
	}
}
/**@fn void functionSwitch4()
 * @brief  Función que controla la tecla 1 de la placa. Aumenta la frecuencia de corte
 * @return None
 */
void functionSwitch4(){
	frecuencia_corte = frecuencia_corte + DELTA_FREC;
	if(frecuencia_corte > FREQUENCY_MAX){
		frecuencia_corte = FREQUENCY_MAX;
	}
}
/** @fn lowPassFilter(uint16_t valor_entrada, uint16_t valor_anterior)
 * @brief Función que aplica un filtro pasa-bajos recibiendo el valor de la señal original y el valor anterior filtrado, y devuelve el valor filtrado
 * @param[in] valor_entrada
 * @param[in] valor_anterior
 * @return valor_salida
 */
uint16_t lowPassFilter(uint16_t valor_entrada, uint16_t valor_anterior){
	float alpha;
	float rc;
	uint16_t valor_salida;

	/* Calculo rc*/
	rc=1/(2*PI*frecuencia_corte);
	/* Calculo alpha*/
	alpha = SAMPLE_DISTANCE/(rc+SAMPLE_DISTANCE);
	/* Filtro pasa-bajos*/
	valor_salida = (float)valor_anterior + alpha*(float)(valor_entrada-valor_anterior);

	return valor_salida;
}


/*==================[external data definition]===============================*/

timer_config my_timer_AD = {TIMER_A,2,analogToDig}; /**< Typedef timer analógico/digital*/
timer_config my_timer_DA = {TIMER_B,5,digToAnalog}; /**< Typedef timer digital/analógico*/
serial_config my_port = {SERIAL_PORT_PC,BAUD_RATE, NO_INT}; /**< Typedef control del puerto*/
analog_input_config my_ad = {CH1,AINPUTS_SINGLE_READ,doAnalogToDig}; /**< Typedef configuración puerto analógico/digital*/

/**@fn void sistemInit()
 * @brief  Función que contiene las funciones que inicializan el sistema
 * @return None
 */
void sistemInit(void)
{
	SystemClockInit();
	SwitchesInit();

	TimerInit(&my_timer_AD);
	TimerStart(TIMER_A);
	AnalogInputInit(&my_ad);
	UartInit(&my_port);

	TimerInit(&my_timer_DA);
	TimerStart(TIMER_B);
	AnalogOutputInit();
}
/*==================[external functions definition]==========================*/
/**@fn void main()
 * @brief  Función main de aplicación del proyecto
 * @return 0
 */
int main(void){

	sistemInit();

	/* Manejo de teclas de la placa por interrupciones */
	SwitchActivInt(SWITCH_1,functionSwitch1);
	SwitchActivInt(SWITCH_2,functionSwitch2);
	SwitchActivInt(SWITCH_3,functionSwitch3);
	SwitchActivInt(SWITCH_4,functionSwitch4);

	/* Variables salida de datos y salida anterior*/
	uint16_t output_data;
	uint16_t previous_data;

    while(1){
    		if(reading_data){

    			if(activate_filter){
    				output_data=lowPassFilter(value,previous_data);
    			}
    			else{
    				/* Si no se aplica el filtro la señal de salida es igual a la señal original */
    				output_data = value;
    			}

    			UartSendString(SERIAL_PORT_PC,UartItoa(value,10));
    			UartSendString(SERIAL_PORT_PC,",");
    			UartSendString(SERIAL_PORT_PC,UartItoa(output_data,10));
    			UartSendString(SERIAL_PORT_PC,"\r");

    			/* Se guarda el valor de la señal en el valor anterior*/
    			previous_data=output_data;

    			reading_data = false;
    		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/

