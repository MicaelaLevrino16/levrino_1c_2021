/*! @mainpage Osciloscopio
 *
 * \section genDesc General Description
 *
 * Permite digitalizar una señal analogica y transmitirla a un graficador de puerto serie de la PC. Además permite aplicar un filtro pasa-bajos a la señal
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIÓMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	   GNDA  	|
 * | 	  PIN2	 	| 	   CH1	    |
 * | 	  PIN3	 	| 	   DAC		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */
#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _OSCILOSCOPIO_PROYECTO4_H */

