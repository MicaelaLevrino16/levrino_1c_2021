/*! @mainpage Parcial Consigna 2
 *
 * \section genDesc General Description
 *
 * Goniómetro digital de rodilla
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIÓMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	   GNDA  	|
 * | 	  PIN2	 	| 	   CH1	    |
 * | 	  PIN3	 	| 	   DAC		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

/*==================[inclusions]=============================================*/
#include "parcial_consigna2.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/** @def BAUD_RATE
 * @brief Número de unidades de señal por segundo
 */
#define BAUD_RATE 115200
/** @def VALOR_MAXIMO
 * @brief Valor máximo que puede tomar la medición
 */
#define VALOR_MAXIMO 1024
/** @def ANGULO_MAXIMO
 * @brief Valor máximo que puede tomar el ángulo
 */
#define ANGULO_MAXIMO 180

/*==================[internal data definition]===============================*/

uint8_t angulo_actual; /**< Valor del ángulo medido*/
uint8_t angulo_maximo_medido; /**Valor del ángulo máximo medido hasta el momento*/
uint16_t value; /**< Valores que toma el potenciómetro*/
bool active=true; /**< Bandera encendido y apagado*/
bool reset=false; /**< Bandera reseteo de la medición*/
bool reading_data = false; /**< Bandera para la lectura de datos*/

/*==================[internal functions declaration]=========================*/

/**@fn void analogToDigital()
 * @brief  Función que inicializa la conversión de analógico a digital
 * @return None
 */
void analogToDigital(void){
	AnalogStartConvertion();
}
/**@fn void doAnalogToDigital()
 * @brief  Función que lee valores analógicos y activa la bandera para la conversión cuando lee el valor
 * @return None
 */
void doAnalogToDigital(void){
	AnalogInputRead(CH1,&value);
	reading_data = true;
}
/**@fn void functionSwitch1()
 * @brief Función que controla la tecla 1 para el encendido.
 * @return None
 */
void functionSwitch1(){
	active = true;
}
/**@fn void functionSwitch2()
 * @brief Función que controla la tecla 2 para el apagado.
 * @return None
 */
void functionSwitch2(){
	active = false;
}
/**@fn void functionSwitch3()
 * @brief Función que controla la tecla 3 para el reseteo de la medición.
 * @return None
 */
void functionSwitch3(){
	reset = true;
}
void calculoAngulo(){
	angulo_actual=(value/VALOR_MAXIMO)*ANGULO_MAXIMO;
}
/**@fn void actualizacionAnguloMaximo()
 * @brief Función que actualiza el valor del ángulo máximo
 * @return None
 */
void actualizacionAnguloMaximo(){
	if(angulo_actual>angulo_maximo_medido){
		angulo_actual=angulo_maximo_medido;
	}
}

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,500,&analogToDigital}; /**< Typedef timer analógico/digital*/
serial_config my_port = {SERIAL_PORT_PC,BAUD_RATE,NO_INT}; /**< Typedef control del puerto*/
analog_input_config my_ad = {CH1,AINPUTS_SINGLE_READ,doAnalogToDigital}; /**< Typedef configuración puerto analógico/digital*/

/**@fn void sistemInit()
 * @brief  Función que realiza las inicializaciones del sistema
 * @return None
 */
void sistemInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	AnalogInputInit(&my_ad);
	UartInit(&my_port);
	SwitchesInit();
	angulo_actual=0;
	angulo_maximo_medido=0;
}

/*==================[external functions definition]==========================*/

/**@fn void main()
 * @brief  Función main de aplicación del proyecto
 * @return 0
 */
int main(void){

	/* Inicialización */
	sistemInit();

	/* Control de teclas por interrupciones*/
	SwitchActivInt(SWITCH_1, functionSwitch1);
	SwitchActivInt(SWITCH_2, functionSwitch2);
	SwitchActivInt(SWITCH_3, functionSwitch3);

    while(1){
    	if(reading_data){
    		if(active==true){
    			calculoAngulo();
    			actualizacionAnguloMaximo();
				UartSendString(SERIAL_PORT_PC, UartItoa(angulo_actual, 10));
				UartSendString(SERIAL_PORT_PC, "°-");
				UartSendString(SERIAL_PORT_PC, UartItoa(angulo_maximo_medido, 10));
				UartSendString(SERIAL_PORT_PC, "°\r\n");
    		}
    		if(active==true && reset==true){
    			angulo_maximo_medido=0;
    			reset=false;
    		}
		reading_data = false;
	    }
	}
    
	return 0;
}

/*==================[end of file]============================================*/

