/*! @mainpage Parcial Consigna 2
 *
 * \section genDesc General Description
 *
 * Goniómetro digital de rodilla
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIÓMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	   GNDA  	|
 * | 	  PIN2	 	| 	   CH1	    |
 * | 	  PIN3	 	| 	   DAC		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Levrino Micaela
 *
 */

#ifndef _PARCIAL_CONSIGNA2_H
#define _PARCIAL_CONSIGNA2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIAL_CONSIGNA2_H */

