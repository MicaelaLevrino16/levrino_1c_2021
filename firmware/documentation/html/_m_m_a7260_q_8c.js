var _m_m_a7260_q_8c =
[
    [ "MAX_VALUE", "_m_m_a7260_q_8c.html#a4ce3e2af80a76d816ab7f8567ec4a65a", null ],
    [ "MAX_VOLTAGE", "_m_m_a7260_q_8c.html#a0637861f9419cef73e2e7e6210280e4e", null ],
    [ "OFFSET", "_m_m_a7260_q_8c.html#a21005f9f4e2ce7597c5eb4351816d7e2", null ],
    [ "SENSITIVITY", "_m_m_a7260_q_8c.html#aa6ac8087ed34271031291f59014fe310", null ],
    [ "MMA7260QDeInit", "_m_m_a7260_q_8c.html#a6c69aff252aa9733ef7911cae3e226e3", null ],
    [ "MMA7260QInit", "group___m_m_a7260_q.html#gaf22ea9d26d18b5f491b7949fe509a699", null ],
    [ "ReadValue", "_m_m_a7260_q_8c.html#af2503d0a760449ff248dba456336aa7d", null ],
    [ "ReadXValue", "group___m_m_a7260_q.html#gac62564c41a96ceca5b38d6b73ef4c6f9", null ],
    [ "ReadYValue", "group___m_m_a7260_q.html#ga25995f82b10255c8466cbb01308ae375", null ],
    [ "ReadZValue", "group___m_m_a7260_q.html#gace645c520fdd92b45b72c711e26fe9e0", null ],
    [ "UnitConvert", "_m_m_a7260_q_8c.html#a3512d44496e0aea11d08f85c219b4074", null ],
    [ "canal", "_m_m_a7260_q_8c.html#a5825c2ddd7aa9484e989c4e864d2ac95", null ],
    [ "conversion", "_m_m_a7260_q_8c.html#a7fbb6f80b04507a51d3dcb3283913db6", null ],
    [ "g_select1_local", "_m_m_a7260_q_8c.html#ae3814b891dc76084e22efb45f3a4a93e", null ],
    [ "g_select2_local", "_m_m_a7260_q_8c.html#a4814e2f3fd4ef30e3398d492301b59f9", null ],
    [ "my_ad_x", "_m_m_a7260_q_8c.html#aa2f4a30ab24d9be5313642058a2c40d8", null ],
    [ "my_ad_y", "_m_m_a7260_q_8c.html#a343a452c79d1c01bda3c7213a30962e9", null ],
    [ "my_ad_z", "_m_m_a7260_q_8c.html#ad07ffc812e17db4d54f77e8148aefb1a", null ],
    [ "valor", "_m_m_a7260_q_8c.html#a6555bc6e0e12aed1d6d19dcd2128e508", null ]
];