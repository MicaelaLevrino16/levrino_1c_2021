/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Proyecto Final 5 - Electrónica Programable", "index.html", [
    [ "Proyecto 5: Acelerómetro aplicado a detección de movimiento de cabeza", "index.html", [
      [ "General Description", "index.html#genDesc", null ],
      [ "Hardware Connection", "index.html#hardConn", null ],
      [ "Changelog", "index.html#changelog", null ]
    ] ],
    [ "MISRA-C:2004 Compliance Exceptions", "_c_m_s_i_s__m_i_s_r_a__exceptions.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_m_s_i_s__m_i_s_r_a__exceptions.html",
"delay_8c.html#a399410bc4062fae6cd67a9296eea434f",
"functions_l.html",
"group___a_d_c__18_x_x__43_x_x.html#gga4cdc8cd4b1ae4721ff5de44c783f310aa44ef9495010c41d5ee491dda81173b66",
"group___c_c_a_n__18_x_x__43_x_x.html#gaa3060227684cfad8ba747a13eae24bdc",
"group___c_h_i_p___s_d_m_m_c___definitions.html#ga984beebb94c1f43f905ebce535601ec1",
"group___c_l_o_c_k__18_x_x__43_x_x.html#gaf5e0d6f4dcbfd5ff64b1ddcd37067ca2",
"group___c_m_s_i_s___core_debug.html#ga0d2907400eb948a4ea3886ca083ec8e3",
"group___c_m_s_i_s___i_t_m.html#ga30e83ebb33aa766070fe3d1f27ae820e",
"group___c_m_s_i_s___s_c_b.html#gada60c92bf88d6fd21a8f49efa4a127b8",
"group___c_m_s_i_s__core___debug_functions.html#ga0374c0b98ab9de6f71fabff7412df832",
"group___c_m_s_i_s__core___debug_functions.html#ga8be676577db129a84a9a2689519a8502",
"group___c_m_s_i_s__core___debug_functions.html#gae8d499140220fa6d4eab1da7262bf08e",
"group___display_i_t_s___e0803.html#ga7a77359e3bccda99b1d684c97f331c16",
"group___e_m_c__18_x_x__43_x_x.html#gaf958769aeac7b6e48484eeca394bea4d",
"group___e_n_e_t__18_x_x__43_x_x.html#ga9354c8fad1b51a076d8cc134e459b5c2",
"group___e_v_r_t__18_x_x__43_x_x.html#ggad3cd727405aa5446c2319375ad1b7276a46c621bfd8fb7612c538d0188e32f5d6",
"group___g_p_d_m_a__18_x_x__43_x_x.html#ga8e1d14364364a15c61d47d644f1abed5",
"group___g_p_i_o_g_p__18_x_x__43_x_x.html#gacb2b28673be061951f30cac631015be1",
"group___i2_c__18_x_x__43_x_x.html#ga36753112210a8c33d566b572b63b753b",
"group___i2_s__18_x_x__43_x_x.html#ga12770c3fc9895b7f777bbc9a333485f8",
"group___i_a_p__18_x_x__43_x_x.html#gab3af925b968fb59823d08d77261aebf7",
"group___o_t_p__18_x_x__43_x_x.html#ga656b4ce60d1b06c4286f395f7dc999b4",
"group___p_e_r_i_p_h__18_x_x___b_a_s_e.html#gaff6de015a63620b4c552891c1ac9f756",
"group___p_i_n_i_n_t__18_x_x__43_x_x.html#ga8d78afc0f4b847de15629e9cd297fd72",
"group___r_t_c__18_x_x__43_x_x.html#ga491df15fba29dd3237f7bd59a9338050",
"group___s_c_t__18_x_x__43_x_x.html#ga0a5b5db9102ca60bfd3f44406ab6763b",
"group___s_d_i_f__18_x_x__43_x_x.html#ga010e118b9ab075a90ed045310efddb09",
"group___s_d_i_f__18_x_x__43_x_x.html#gaec05814b0956550eb414ed61a2845eb6",
"group___s_s_p__18_x_x__43_x_x.html#ga10f56047b6024ff848675f9463f1b989",
"group___s_s_p__18_x_x__43_x_x.html#ggafaff4574b830e5b94dcaf7ca8da399e8a05e1bf0ddb0306501fcb234c6e885082",
"group___u_a_r_t.html#ga24a5418ce90e4d3f4d5dbfcdf2d2313d",
"group___u_a_r_t__18_x_x__43_x_x.html#ga9fdc7f45b2fb3679b64164b34afb9350",
"group___u_s_b_d___core.html#gaa578d29a85226108ef62c6d5c325b742",
"group___u_s_b_d___h_i_d.html#ga4ce2a1dddd5fa7318aaec83b1bdebccf",
"group___u_s_b_d___h_i_d.html#gad8897aa24dd4a36d8efb6fafede2b015",
"i2c__18xx__43xx_8h.html#ga5fb1ba338fb3822bb6ca012adc4194bfab6d7a585cfa1106eb290fa9f302c5783",
"struct___u_s_b___d_e_v_i_c_e___d_e_s_c_r_i_p_t_o_r.html#a1deca1f1d6e5815b290e6e1015bce5b8",
"struct_i_p___e_m_c___s_t_a_t_i_c___c_o_n_f_i_g___t.html#a4ce7377178b8c0ee57726e8a73c06e66",
"struct_l_p_c___e_n_e_t___t.html#ad167767881a51644b80b26acf8d31f31",
"struct_l_p_c___r_o_m___a_p_i___t.html",
"struct_o_t_p___a_p_i___t.html#a5d8400d1ec09241959b5e048826c4ae8",
"timer_8c.html#a0cf866147f6b8097318ea162c2475649",
"usbd__adc_8h.html#ae47adfd75060b9d251b58281e358cee0",
"usbd__msc_8h.html#ae21ca4053c3b176401d7caa1df639d95"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';