var searchData=
[
  ['delayms_6751',['DelayMs',['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c'],['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c']]],
  ['delaysec_6752',['DelaySec',['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c'],['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c']]],
  ['delayus_6753',['DelayUs',['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c'],['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c']]],
  ['detectahead_6754',['detectAhead',['../proyecto5_8c.html#a3fe68549674dd90aa99452a6512d4a0d',1,'proyecto5.c']]],
  ['detectionkeep_6755',['detectionKeep',['../proyecto5_8c.html#a62299f5a99e2cef28342110fd16317e5',1,'proyecto5.c']]],
  ['detectleft_6756',['detectLeft',['../proyecto5_8c.html#adbcb5b733691d95613ba5c54a46e0a51',1,'proyecto5.c']]],
  ['detectright_6757',['detectRight',['../proyecto5_8c.html#a7fa41596dbd217c2bc44ec4293a0f7c6',1,'proyecto5.c']]],
  ['disableclk_6758',['disableClk',['../i2c__18xx__43xx_8c.html#a8e3474e1fe1ce21215ce79bccbf1948a',1,'i2c_18xx_43xx.c']]]
];
