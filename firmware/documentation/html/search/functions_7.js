var searchData=
[
  ['initdynmem_6797',['initDynMem',['../emc__18xx__43xx_8c.html#aaee0a1bdfbf4b5e3212f3ef8498afe4a',1,'emc_18xx_43xx.c']]],
  ['initstaticmem_6798',['initStaticMem',['../emc__18xx__43xx_8c.html#ac263a3e68931d182564bca53b499021b',1,'emc_18xx_43xx.c']]],
  ['isi2cbusfree_6799',['isI2CBusFree',['../i2c__18xx__43xx_8c.html#adf0dffdb74771c1f98a5e959e6dca498',1,'i2c_18xx_43xx.c']]],
  ['ismasterstate_6800',['isMasterState',['../i2c__18xx__43xx_8c.html#aa5d73e6cbd7622d475d0d050c68a6c4c',1,'i2c_18xx_43xx.c']]],
  ['isslaveaddrmatching_6801',['isSlaveAddrMatching',['../i2c__18xx__43xx_8c.html#a4c1168486165c01fd04e1d916fbd1219',1,'i2c_18xx_43xx.c']]],
  ['itm_5fcheckchar_6802',['ITM_CheckChar',['../group___c_m_s_i_s__core___debug_functions.html#gae61ce9ca5917735325cd93b0fb21dd29',1,'core_cm4.h']]],
  ['itm_5freceivechar_6803',['ITM_ReceiveChar',['../group___c_m_s_i_s__core___debug_functions.html#gac3ee2c30a1ac4ed34c8a866a17decd53',1,'core_cm4.h']]],
  ['itm_5fsendchar_6804',['ITM_SendChar',['../group___c_m_s_i_s__core___debug_functions.html#gac90a497bd64286b84552c2c553d3419e',1,'core_cm4.h']]],
  ['itse0803deinit_6805',['ITSE0803Deinit',['../group___display_i_t_s___e0803.html#ga261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../group___display_i_t_s___e0803.html#ga261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803displayvalue_6806',['ITSE0803DisplayValue',['../group___display_i_t_s___e0803.html#ga7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c'],['../group___display_i_t_s___e0803.html#ga7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c']]],
  ['itse0803init_6807',['ITSE0803Init',['../group___display_i_t_s___e0803.html#ga5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../group___display_i_t_s___e0803.html#ga5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803readvalue_6808',['ITSE0803ReadValue',['../group___display_i_t_s___e0803.html#ga3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c'],['../group___display_i_t_s___e0803.html#ga3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c']]]
];
