var searchData=
[
  ['readadcval_6835',['readAdcVal',['../adc__18xx__43xx_8c.html#a8edc226a08b05b47c994049bcd00c60c',1,'adc_18xx_43xx.c']]],
  ['readvalue_6836',['ReadValue',['../_m_m_a7260_q_8c.html#af2503d0a760449ff248dba456336aa7d',1,'MMA7260Q.c']]],
  ['readxvalue_6837',['ReadXValue',['../group___m_m_a7260_q.html#gac62564c41a96ceca5b38d6b73ef4c6f9',1,'ReadXValue():&#160;MMA7260Q.c'],['../group___m_m_a7260_q.html#gac62564c41a96ceca5b38d6b73ef4c6f9',1,'ReadXValue():&#160;MMA7260Q.c']]],
  ['readyvalue_6838',['ReadYValue',['../group___m_m_a7260_q.html#ga25995f82b10255c8466cbb01308ae375',1,'ReadYValue():&#160;MMA7260Q.c'],['../group___m_m_a7260_q.html#ga25995f82b10255c8466cbb01308ae375',1,'ReadYValue():&#160;MMA7260Q.c']]],
  ['readzvalue_6839',['ReadZValue',['../group___m_m_a7260_q.html#gace645c520fdd92b45b72c711e26fe9e0',1,'ReadZValue():&#160;MMA7260Q.c'],['../group___m_m_a7260_q.html#gace645c520fdd92b45b72c711e26fe9e0',1,'ReadZValue():&#160;MMA7260Q.c']]],
  ['reset_6840',['reset',['../enet__18xx__43xx_8c.html#aeb253604a11400185c3a9933e18c68c3',1,'enet_18xx_43xx.c']]],
  ['ringbuffer_5fflush_6841',['RingBuffer_Flush',['../group___ring___buffer.html#ga5f66a5dd980ef03877cf8e0c96ad4ebb',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetcount_6842',['RingBuffer_GetCount',['../group___ring___buffer.html#ga7b69777c35694637acaf39e6bfcc1822',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetfree_6843',['RingBuffer_GetFree',['../group___ring___buffer.html#ga75424687def8979742338366d39c8559',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetsize_6844',['RingBuffer_GetSize',['../group___ring___buffer.html#ga2fc4b40b03afb19c8ea942da3cf3faf1',1,'ring_buffer.h']]],
  ['ringbuffer_5finit_6845',['RingBuffer_Init',['../group___ring___buffer.html#gaaf3bb51f2228ea1bea603e19c7eba5bb',1,'RingBuffer_Init(RINGBUFF_T *RingBuff, void *buffer, int itemSize, int count):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaaf3bb51f2228ea1bea603e19c7eba5bb',1,'RingBuffer_Init(RINGBUFF_T *RingBuff, void *buffer, int itemSize, int count):&#160;ring_buffer.c']]],
  ['ringbuffer_5finsert_6846',['RingBuffer_Insert',['../group___ring___buffer.html#gaafdee54f2525b2c7a983d1a631b42226',1,'RingBuffer_Insert(RINGBUFF_T *RingBuff, const void *data):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaafdee54f2525b2c7a983d1a631b42226',1,'RingBuffer_Insert(RINGBUFF_T *RingBuff, const void *data):&#160;ring_buffer.c']]],
  ['ringbuffer_5finsertmult_6847',['RingBuffer_InsertMult',['../group___ring___buffer.html#gafeafb521d4e03052ab2c893fd0e388d5',1,'RingBuffer_InsertMult(RINGBUFF_T *RingBuff, const void *data, int num):&#160;ring_buffer.c'],['../group___ring___buffer.html#gafeafb521d4e03052ab2c893fd0e388d5',1,'RingBuffer_InsertMult(RINGBUFF_T *RingBuff, const void *data, int num):&#160;ring_buffer.c']]],
  ['ringbuffer_5fisempty_6848',['RingBuffer_IsEmpty',['../group___ring___buffer.html#ga6f03e04a69262864bde4f35fc6f3dfb5',1,'ring_buffer.h']]],
  ['ringbuffer_5fisfull_6849',['RingBuffer_IsFull',['../group___ring___buffer.html#ga760da012435262add1d8d7aa79e873a0',1,'ring_buffer.h']]],
  ['ringbuffer_5fpop_6850',['RingBuffer_Pop',['../group___ring___buffer.html#gaf3ce7f43677c2b4c6eedb3cc4962b80d',1,'RingBuffer_Pop(RINGBUFF_T *RingBuff, void *data):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaf3ce7f43677c2b4c6eedb3cc4962b80d',1,'RingBuffer_Pop(RINGBUFF_T *RingBuff, void *data):&#160;ring_buffer.c']]],
  ['ringbuffer_5fpopmult_6851',['RingBuffer_PopMult',['../group___ring___buffer.html#gae0ef7bb96d1fe84ae1441b7c214b1e56',1,'RingBuffer_PopMult(RINGBUFF_T *RingBuff, void *data, int num):&#160;ring_buffer.c'],['../group___ring___buffer.html#gae0ef7bb96d1fe84ae1441b7c214b1e56',1,'RingBuffer_PopMult(RINGBUFF_T *RingBuff, void *data, int num):&#160;ring_buffer.c']]],
  ['rit_5firqhandler_6852',['RIT_IRQHandler',['../timer_8c.html#a94381ae71a78d9fcc2b50d0b13a5b45e',1,'timer.c']]]
];
