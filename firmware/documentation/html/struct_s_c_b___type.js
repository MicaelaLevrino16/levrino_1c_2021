var struct_s_c_b___type =
[
    [ "ADR", "group___c_m_s_i_s__core___debug_functions.html#ga5c0e2e1c7195d4dc09a5ca077c596318", null ],
    [ "AFSR", "group___c_m_s_i_s__core___debug_functions.html#gab9176079ea223dd8902589da91af63a2", null ],
    [ "AIRCR", "group___c_m_s_i_s__core___debug_functions.html#gaaec159b48828355cb770049b8b2e8d91", null ],
    [ "BFAR", "group___c_m_s_i_s__core___debug_functions.html#gad49f99b1c83dcab356579af171bfa475", null ],
    [ "CCR", "group___c_m_s_i_s__core___debug_functions.html#ga5e1322e27c40bf91d172f9673f205c97", null ],
    [ "CFSR", "group___c_m_s_i_s__core___debug_functions.html#gae6b1e9cde3f94195206c016214cf3936", null ],
    [ "CPACR", "group___c_m_s_i_s__core___debug_functions.html#gacccaf5688449c8253e9952ddc2161528", null ],
    [ "CPUID", "group___c_m_s_i_s__core___debug_functions.html#ga30abfea43143a424074f682bd61eace0", null ],
    [ "DFR", "group___c_m_s_i_s__core___debug_functions.html#ga1b9a71780ae327f1f337a2176b777618", null ],
    [ "DFSR", "group___c_m_s_i_s__core___debug_functions.html#ga415598d9009bb3ffe9f35e03e5a386fe", null ],
    [ "HFSR", "group___c_m_s_i_s__core___debug_functions.html#ga87aadbc5e1ffb76d755cf13f4721ae71", null ],
    [ "ICSR", "group___c_m_s_i_s__core___debug_functions.html#ga8fec9e122b923822e7f951cd48cf1d47", null ],
    [ "ISAR", "group___c_m_s_i_s__core___debug_functions.html#ga130a0c6b3da7f29507a1888afbdce7ee", null ],
    [ "MMFAR", "group___c_m_s_i_s__core___debug_functions.html#ga88820a178974aa7b7927155cee5c47ed", null ],
    [ "MMFR", "group___c_m_s_i_s__core___debug_functions.html#gab0dc71239f7d5ffe2e78e683b9530064", null ],
    [ "PFR", "group___c_m_s_i_s__core___debug_functions.html#ga00a6649cfac6bbadee51d6ba4c73001d", null ],
    [ "RESERVED0", "group___c_m_s_i_s__core___debug_functions.html#ga92bdd34d0bb3e2d14a3ce60040036510", null ],
    [ "SCR", "group___c_m_s_i_s__core___debug_functions.html#ga64a95891ad3e904dd5548112539c1c98", null ],
    [ "SHCSR", "group___c_m_s_i_s__core___debug_functions.html#ga04d136e5436e5fa2fb2aaa78a5f86b19", null ],
    [ "SHP", "group___c_m_s_i_s__core___debug_functions.html#ga17dc9f83c53cbf7fa249e79a2d2a43f8", null ],
    [ "VTOR", "group___c_m_s_i_s__core___debug_functions.html#gaaf388a921a016cae590cfcf1e43b1cdf", null ]
];