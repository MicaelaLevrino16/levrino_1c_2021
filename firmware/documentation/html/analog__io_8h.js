var analog__io_8h =
[
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "AINPUTS_BURST_READ", "analog__io_8h.html#a9439488d01a948fab1b2f8bc0bab00fa", null ],
    [ "AINPUTS_SINGLE_READ", "analog__io_8h.html#a5b01b5f293bd90a7bf9f4a988b3fce04", null ],
    [ "ANALOG_INPUT_READ", "analog__io_8h.html#a7b3118011a8b70756613058be97884ab", null ],
    [ "ANALOG_OUTPUT_RANGE", "analog__io_8h.html#a950c25493cc190f761c14db95326beee", null ],
    [ "ANALOG_OUTPUT_WROTE", "analog__io_8h.html#a731c5701b891f35c7f95766fdda7aa9b", null ],
    [ "CH0", "analog__io_8h.html#af6f592bd18a57b35061f111d32a7f637", null ],
    [ "CH1", "analog__io_8h.html#a90643238cf4f49aae5f0ab215ada7889", null ],
    [ "CH2", "analog__io_8h.html#a92dc73af14a6902eadd21bdee033cbfb", null ],
    [ "CH3", "analog__io_8h.html#a9a593b4f2e9cc1ab563a99ccc361ac2e", null ],
    [ "DAC", "analog__io_8h.html#a4aa2a4ab86ce00c23035e5cee2e7fc7e", null ],
    [ "ERROR_CHANNEL_SELECTION", "analog__io_8h.html#ac826d6653b6cb9c76e25f9b767f6eafa", null ],
    [ "AnalogInputInit", "analog__io_8h.html#a75e49898ce3cf18d67e4d463c6e2a8de", null ],
    [ "AnalogInputRead", "analog__io_8h.html#ad13e6436e0177f0e17dc1a01fc4d47af", null ],
    [ "AnalogInputReadPolling", "analog__io_8h.html#a1673192453e03e9fab4976534b20f3e9", null ],
    [ "AnalogOutputInit", "analog__io_8h.html#ab57399e946247652a096a0e2d3a1b69a", null ],
    [ "AnalogOutputWrite", "analog__io_8h.html#a464364f0790a3e7d1ab9d9e9c2d9092c", null ],
    [ "AnalogStartConvertion", "analog__io_8h.html#a551f68a70593d1b97e8c41a470707368", null ]
];