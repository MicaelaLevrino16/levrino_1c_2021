var parcial__consigna2_8c =
[
    [ "ANGULO_MAXIMO", "parcial__consigna2_8c.html#a3cfec38fc3a991df8ece334d4739d6b4", null ],
    [ "BAUD_RATE", "parcial__consigna2_8c.html#ad4455691936f92fdd6c37566fc58ba1f", null ],
    [ "VALOR_MAXIMO", "parcial__consigna2_8c.html#a1622f9310b9f2345c8e9494bd4851d13", null ],
    [ "actualizacionAnguloMaximo", "parcial__consigna2_8c.html#a9f6ff8c96cb5a5d0bf61d24ba614b54e", null ],
    [ "analogToDigital", "parcial__consigna2_8c.html#a6bf5287942dda748a4272d1e57baef52", null ],
    [ "calculoAngulo", "parcial__consigna2_8c.html#a75a4e7960b6072e9aa5555c93d46d1cb", null ],
    [ "doAnalogToDigital", "parcial__consigna2_8c.html#a82f62123830a760e7126ec27b90587ee", null ],
    [ "functionSwitch1", "parcial__consigna2_8c.html#a7cf6c5d1522cb7debedeeace46103bff", null ],
    [ "functionSwitch2", "parcial__consigna2_8c.html#a249a201904e2e8ac8b62ca96965fcc55", null ],
    [ "functionSwitch3", "parcial__consigna2_8c.html#a4703ceb659149175ed232f67e52e8757", null ],
    [ "main", "parcial__consigna2_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "sistemInit", "parcial__consigna2_8c.html#a6cffba175a10cc89c5e6ec038c2385a2", null ],
    [ "active", "parcial__consigna2_8c.html#a03c996f9fcf0e10baeb3e700be0c409a", null ],
    [ "angulo_actual", "parcial__consigna2_8c.html#a9dc19a51e9a80a8208c6e153ea8ff211", null ],
    [ "angulo_maximo_medido", "parcial__consigna2_8c.html#adbd17601d1afb13b8580a2ea370dc5fe", null ],
    [ "my_ad", "parcial__consigna2_8c.html#a8cd1fa2a0b77ad966b0653a113e64ae1", null ],
    [ "my_port", "parcial__consigna2_8c.html#a7c2fab8a7d4c6735daf4f241984f4187", null ],
    [ "my_timer", "parcial__consigna2_8c.html#aeaf95171809a97992e2f7a291aceecb6", null ],
    [ "reading_data", "parcial__consigna2_8c.html#a2d386cab9d48cb607751f81453bf1751", null ],
    [ "reset", "parcial__consigna2_8c.html#a8409a0d351d4fba2eeef26a7dcc585a8", null ],
    [ "value", "parcial__consigna2_8c.html#a900b69af7d674b8db3bceae754b4955a", null ]
];