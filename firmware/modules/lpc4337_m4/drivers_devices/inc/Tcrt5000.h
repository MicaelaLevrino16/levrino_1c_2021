/* Copyright 2021,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TCRT5000_H
#define TCRT5000_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Tcrt5000
 ** @{ */

/** \brief driver capable to detect and count compartments.
 **
 ** This is a driver for a TCRT5000 sensor connected to the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200415 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/

#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn bool Tcrt5000Init(gpio_t dout)
 * @brief Initialization function to control the object counter on the conveyor belt on the EDU-CIAA BOARD
 * This function initialize the peripheral dout as an input periphery.
 * @param[in] dout
 * @return 1 (true) if no error
 */
bool Tcrt5000Init(gpio_t dout);
/** @fn bool Tcrt5000State(void)
 * @brief Function to read counter status
 * This function returns true if it detects an object.
 * @param[in] No parameter
 * @return counter status
 */
bool Tcrt5000State(void);
/** @fn bool Tcrt5000Deinit(gpio_t dout)
 * @brief Function to de-initialize
 * This function de-initialize the peripheral dout.
 * @param[in] dout
 * @return 1 (true) if no error
 */
bool Tcrt5000Deinit(gpio_t dout);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef TCRT5000_H */

