/* Copyright 2021,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef  DISPLAYITS_E0803_H
#define  DISPLAYITS_E0803_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DisplayITS_E0803
 ** @{ */

/** @brief driver capable of controls a 3-digit LCD output interface.
 **
 ** This driver displays a decimal number sent through 4 port lines and 3 control lines
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200415 v0.0.1 initials initial version
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief Initialization function of EDU-CIAA pins GPIO
 * This function initialize the peripherals as an output periphery.
 * @param[in] pins
 * @return TRUE if no error
 */
bool ITSE0803Init(gpio_t * pins);

 /** @fn bool ITSE0803DisplayValue(uint16_t valor);
  * @brief Shows the value of the decimal number on the display
  * @param[in] valor
  * @return FALSE if an error occurs, in other case returns TRUE
  */
bool ITSE0803DisplayValue(uint16_t valor);

/** @fn uint16_t ITSE0803ReadValue(void)
 * @brief Read the value of the decimal number
 * @param[in] No parameter
 * @return uint16_t with the numerical value that is being displayed on the LCD screen
 */
uint16_t ITSE0803ReadValue(void);

/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief Function to de-initialize
 * This function de-initialize the peripherals.
 * @param[in] pins
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool ITSE0803Deinit(gpio_t * pins);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
#endif /* #ifndef DISPLAYITS_E0803_H */

