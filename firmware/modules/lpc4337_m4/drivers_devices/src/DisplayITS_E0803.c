/* Copyright 2020,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief driver capable of controls a 3-digit LCD output interface.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200415 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
gpio_t V_PINS[7];
bool init_display=false;
uint8_t bcd_number[3];
uint16_t num;


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/



bool ITSE0803Init(gpio_t * pins){
	init_display = 1; /* true*/
	for(int i=0;i<7;i++){
		GPIOInit(pins[i], GPIO_OUTPUT);
		V_PINS[i]=pins[i];
	}
	return init_display;
}

bool ITSE0803DisplayValue(uint16_t valor)
{
	uint8_t i,j,k;
	if(valor<=999){
		num=valor;
		for(i=0;i<3;i++){
			bcd_number[i]=valor%10;
			valor=valor/10;
		}
		for(j=0; j<3; j++){
			for(k=0; k<4; k++){
				if((bcd_number[j] & (1<< k))){
					GPIOOn(V_PINS[k]);
				}
				else{
					GPIOOff(V_PINS[k]);
					}
				}
				GPIOOn(V_PINS[6-j]);
				GPIOOff(V_PINS[6-j]);
		}
	}
	return 1;
}

uint16_t ITSE0803ReadValue(void){
	return num;
}

bool ITSE0803Deinit(gpio_t * pins)
{
	GPIODeinit();
	return 1;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
