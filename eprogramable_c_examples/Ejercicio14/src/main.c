/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Levrino Micaela
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/

/*EJERCICIO 14. Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
*a) Defina una variable con esa estructura y cargue los campos con sus propios datos.
*b) Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).*/

typedef struct alumno {
	uint8_t apellido[20];
	uint8_t nombre[12];
	uint8_t edad;
} alumno;

alumno alumno_1;
alumno *alum; /* Puntero alias a alumno_2 */
alumno alumno_2;
uint8_t ape[] = "Levrino";
uint8_t nom[] = "Micaela";
uint8_t ape2[] = "Avetta";
uint8_t nom2[] = "Valentina";

/*==================[internal functions declaration]=========================*/

int main(void)
{
	alum = &alumno_2; /* alum es un puntero alias de alumno 2*/

    alumno_1.edad = 22;

    alum->edad = 22; /* Se accede a la estructura mediante punteros */

    strcpy(alumno_1.apellido,ape);
    strcpy(alumno_1.nombre,nom);

    strcpy(alum->apellido,ape2);
    strcpy(alum->nombre,nom2);


    /* Se muestran datos alumno 1*/
    printf("Edad de la alumna 1: %d \r\n", alumno_1.edad);
    printf("Apellido de la alumna 1: %s \r\n", alumno_1.apellido);
    printf("Nombre de la alumna 1: %s \r\n\r\n", alumno_1.nombre);

    /* Se muestran datos alumnos 2*/
    printf("Edad de la alumna 2: %d \r\n", alumno_2.edad);
    printf("Apellido de la alumna 2: %s \r\n", alumno_2.apellido);
    printf("Nombre de la alumna 2: %s \r\n", alumno_2.nombre);

    return 0;
}

/*==================[end of file]============================================*/

