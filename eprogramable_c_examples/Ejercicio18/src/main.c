/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Levrino Micaela
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

/* EJERCICIO 18. Al ejercicio anterior agregue un número más (16 en total), modifique su programa de manera que agregue
 *el número extra a la suma e intente no utilizar el operando división “/” N°16 =>233. Si utiliza las directivas de
 *el preprocesador ( #ifdef, #ifndef,#endif, etc) no necesita generar un nuevo programa. */

#define CANTIDAD 16

/*==================[internal functions declaration]=========================*/
const uint8_t numbers[]={234, 123, 111, 101, 32, 116, 211, 24, 214, 100, 124, 222, 1, 129, 9, 233};

int main(void)
{
	uint16_t sum=0;
	uint8_t i,average=0;

	for(i=0; i<CANTIDAD; i++) /* Se suman todos los números y se acumulan en una variable*/
		{
			sum+=numbers[i];
			printf("N°%d: %d \n",i+1,numbers[i]);
		}

	average=(uint8_t)(sum>>4); /*Divido la suma corriendo 4 lugares a la derecha*/

	printf("Promedio en enteros: %d",average);

	return 0;
}

/*==================[end of file]============================================*/

