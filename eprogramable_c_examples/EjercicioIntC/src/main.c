/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Levrino Micaela
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

/* EJERCICIO C. Escriba una función que reciba un dato de 32 bits,  la cantidad
 * de dígitos de salida y un puntero a un arreglo donde se almacene los n dígitos.
 * La función deberá convertir el dato recibido a BCD, guardando cada uno de los
 * dígitos de salida en el arreglo pasado como puntero.*/

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	/* Numero a convertir 1234, dígito 4*/
	uint8_t i;
	for(i=0; i<digits; i++)
	{
	    //while(digits > 0){ /* Mientras tenga digitos*/ Caso usando el while en cambio de for
		//digits--;
		printf("Digito: %d \r\n", i);
		bcd_number[i] = data%10; /* Guardo en BCD el valor*/
		printf("Resto: %d \r\n", bcd_number[i]);
		data = data/10;
	}
}

/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint32_t dato = 1234; /* Numero de prueba*/
    uint8_t numbers[4]; /* En numbers está cada una de las cifras*/
    BinaryToBcd(dato, 4, numbers);
    printf("Digito en la posición 2: %d \r\n", numbers[2]); /*Se muestra un digito como ejemplo de que funciona correctamente la función*/

	return 0;
}

/*==================[end of file]============================================*/

