/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Levrino Micaela
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

/* EJERCICIO A. Realice un función que reciba un puntero a una estructura LED*/

typedef enum{
	ON=1, /* Forzamos a que arranque en 1 y no en cero*/
	OFF,
	TOGGLE,
}state_t1;

typedef enum{
	Led_1=1, /* Forzamos a que arranque en 1 y no en cero*/
	Led_2,
	Led_3,
}state_t;

typedef struct
{
	uint8_t n_led;      /* indica el número de led a controlar*/
	uint8_t n_ciclos;   /* indica la cantidad de ciclos de encendido/apagado*/
	uint8_t periodo;    /* indica el tiempo de cada ciclo*/
	uint8_t mode;       /* ON, OFF, TOGGLE*/
} leds; /* Se crea tipo de variable leds*/

void LedControl(leds * l);

/*==================[internal functions declaration]=========================*/

void LedControl(leds * l){
	uint8_t i,j;
	switch(l->mode){
	case ON:
		switch(l->n_led){
		case Led_1:
			printf("Se enciende led %d \n", Led_1);
			break;
		case Led_2:
			printf("Se enciende led %d \n", Led_2);
			break;
		case Led_3:
			printf("Se enciende led %d \n", Led_3);
			break;
		}
		break;
	case OFF:
		switch(l->n_led){
		case Led_1:
			printf("Se apaga led %d \n", Led_1);
			break;
		case Led_2:
			printf("Se apaga led %d \n", Led_2);
			break;
		case Led_3:
			printf("Se apaga led %d \n", Led_3);
			break;
		}
		break;
	case TOGGLE:
		for(i=0; i<l->n_ciclos ;i++){
			switch(l->n_led){
			case Led_1:
				printf("Se togglea led %d \n", Led_1);
				break;
			case Led_2:
				printf("Se togglea led %d \n", Led_2);
				break;
			case Led_3:
				printf("Se togglea led %d \n", Led_3);
				break;
			}
			for(j=0; j<l->periodo; j++){} /*Retardo de periodo de tiempo*/
		}
		break;
	}
}

int main(void)
{
	leds my_led={Led_1, 10, 100, TOGGLE}; /* Se inicializa una estructura de prueba led 1*/
	LedControl(&my_led); /* Se le pasa dirección de memoria de dónde está el led*/

	return 0;
}

/*==================[end of file]============================================*/

